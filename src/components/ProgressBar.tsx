/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Saturday December 26th 2020
 */


import * as React from 'react';

import './ProgressBar.scss';

/****************
 * 
 * 
 * 
interface ColumnProps {
  text: string
  index: number
  id: string
  isPreview?: boolean
}

export const Column = ({ text, index, id, isPreview }: ColumnProps) => {


    
export default ({ activeStepIndex }) => {
 */
//export const ProgressBar = ( activeStepIndex: number ) => {

interface ColumnProps {
    activeStepIndex: number
    }
    
export const ProgressBar=({activeStepIndex}: ColumnProps) => {
  const getStepIndicatorClass = (index: number, activeIndex: number) =>
    index === activeIndex ? 'step-indicator active' : 'step-indicator';

  const steps = [{
    index: 1,
    label: 'Animal'
  }, {
    index: 2,
    label: 'Breed'
  }, {
    index: 3,
    label: 'Picture'
  }];

  return (
    <ul id="progressbar">
      {
        steps.map(step =>
          <li className={getStepIndicatorClass(step.index, activeStepIndex)} key={step.index}>
            {step.label}
          </li>
        )
      }
    </ul>
  );
};