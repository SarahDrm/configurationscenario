/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Monday December 28th 2020
 */


import React, { FunctionComponent } from 'react'
import { ReactEventHandler } from 'react'
import { Link } from 'react-router-dom'

/**
 * 
 * @param param0 data
 * param cliquable or not
 */
 
interface ListProps {
    text: string,
    isClickable: boolean,
    urlLink: string
    //onClick:  ReactEventHandler
  }

//export default function ClientSiteElements(props:any): JSX.Element {
    
export const List: FunctionComponent<ListProps> = ({
    text, 
    isClickable,
    urlLink
    //onClick
    }) => {  
        return(
            <div>
            {/* {(clickable)?  <li onClick={onClick}> {text}</li> : <li> {text}</li> } */}
            <Link to={isClickable ? urlLink : '#'} >{text}</Link>
            <Link to={urlLink} >{text}</Link>
           
            </div>
        )       
    }

// const List = ({ data, clickable: boolean }) => (
//     { 
//         (clickable)?  <li onClick={onClick}> {data}</li> : <li onClick={onClick}> {text}</li> 
//     }

// )

// List.propTypes = {
//   onClick: PropTypes.func.isRequired,
//   completed: PropTypes.bool.isRequired,
//   text: PropTypes.string.isRequired
// }

export default List
