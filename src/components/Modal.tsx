/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Monday December 28th 2020
 */

//import { Button } from "@material-ui/core";
import React, { useState } from "react";
import { Button, Modal } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";

 export default function Modale(props:any): JSX.Element {

  const [showModal, setShowModal] = useState<boolean>(false);

  const close = () => {   setShowModal(false) }

  const open = () => {    setShowModal(true)  }
    
    return (
      <div>
        <Button
          onClick={open}
        >
          Launch demo modal
        </Button>

         <Modal show={showModal} onHide={close}>
            <Modal.Header closeButton>
                <Modal.Title>Modal heading</Modal.Title>
            </Modal.Header>
            <Modal.Body>Woohoo, you're reading this text in a modal!</Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={close}>
                    Close
                </Button>
                <Button variant="primary" onClick={close}>
                    Save Changes
                </Button>
            </Modal.Footer>
        </Modal>
      </div>
    ); 
}

