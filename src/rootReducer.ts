/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Saturday December 26th 2020
 */

import { combineReducers } from '@reduxjs/toolkit'

import site from './features/Site/slice';
import contactList from './features/Site/Contacts/slice';
import deviceGroup from './features/Site/Devices/slice';
import scenarios from './features/Site/Scenarios/slice';
import sources from './features/Site/Scenarios/Sources/slice';
import deviceGroupLicences from './features/Site/Devices/licence/slice';
import sourcesEvents from './features/Site/Scenarios/Sources/Events/slice';

const rootReducer = combineReducers({
  site,
  contactList,
  deviceGroup,
  scenarios,
  sources,
  deviceGroupLicences,
  sourcesEvents
})

export type RootState = ReturnType<typeof rootReducer>

export default rootReducer

