/*
 * Copyright (c) 2021 MyKeeper
 * -----
 * Created Date: Tuesday January 5th 2021
 */


/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Thursday December 31st 2020
 */


import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { AppThunk } from '../../../../store'
import { Licence } from '../../../../dataModel/Licence';
import { getLicensesOfDeviceTypeFromApi } from './api';
import { RootState } from '../../../../rootReducer';

const initialState : any = ({
   error:null,
   pending: true,
   licencesOfDeviceType:[]
});  

const slice = createSlice({
    name: 'deviceGroupLicences',
    initialState,
    reducers: {
          getLicensesOfDeviceTypeAction(state, action: PayloadAction<Licence[]>) {
            state.licencesOfDeviceType = action.payload;
          }
    }
});

export const getLicensesOfDeviceType = (deviceType: string): AppThunk => async (dispatch) => {
  const licenceLists: Licence[] = await getLicensesOfDeviceTypeFromApi(deviceType);
  dispatch(slice.actions.getLicensesOfDeviceTypeAction(licenceLists))
}

export const { getLicensesOfDeviceTypeAction } = slice.actions; 

export const selectDeviceGroupsLicences = (state: RootState): Licence[] => state.deviceGroupLicences.licencesOfDeviceType


export default slice.reducer; 
				  