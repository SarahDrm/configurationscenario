/*
 * Copyright (c) 2021 MyKeeper
 * -----
 * Created Date: Tuesday January 5th 2021
 */

import axios from "axios";
import { api_url } from "../../../../api/apiAuth";
import { Licence } from "../../../../dataModel/Licence";

const feature = "license/";

export async function getLicensesOfDeviceTypeFromApi(deviceType: string) : Promise<Licence[]> {
    const data = {
        deviceType: deviceType
    };
    try {
        const result = await axios.post(`${api_url}${feature}getLicensesOfDeviceType`,data);
        return result.data.result
    } catch (err) {
        console.log(err.toString());
        return [];
    }
}