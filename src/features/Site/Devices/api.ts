/*
 * Copyright (c) 2021 MyKeeper
 * -----
 * Created Date: Tuesday January 5th 2021
 */

import axios from "axios";
import { api_url } from "../../../api/apiAuth";
import { DeviceGroup } from "../../../dataModel/DeviceGroup";


const feature = "device/";

export async function getDeviceGroupsFromApi(siteId: string) : Promise<DeviceGroup[]> {
    const data = {
        siteId: siteId
    };
    try {
        const result = await axios.post(`${api_url}${feature}getDeviceGroupBySiteId`,data);
        return result.data.result
    } catch (err) {
        console.log(err.toString());
        return [];
    }
}


export async function addDeviceGroupWithApi(siteId: string, name: string, deviceType:string, deviceLicence:string) : Promise<DeviceGroup[]> {
    const data =   {
        siteId: siteId,
        name: name ,
        deviceType: deviceType,
        deviceLicence: deviceLicence
      }
    try { 
        const result = await axios.post(`${api_url}${feature}createDeviceGroup`,data);
        return result.data.result
      } catch (err) {
        console.log(err.toString());
        return [];
      }
}




