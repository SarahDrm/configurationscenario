/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Thursday December 31st 2020
 */


import React, { FunctionComponent } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../../rootReducer';
import {  
    getDeviceGroups, 
   // getLicensesOfDeviceType, 
    setDeviceType } from './slice'
import { DeviceGroup } from '../../../dataModel/DeviceGroup';
import ModaleDevice from './ModaleDevice';
import { Licence } from '../../../dataModel/Licence';
import { getLicensesOfDeviceType, selectDeviceGroupsLicences } from './licence/slice';

interface SiteProps {
    siteId: string
  }
    
export const DeviceGroups: FunctionComponent<SiteProps> = ({siteId}) => {  

    const dispatch = useDispatch();
       
    const deviceGroups = useSelector( (state: RootState) => state.deviceGroup.deviceGroups );

    const deviceType = useSelector( (state: RootState) => state.deviceGroup.deviceType );

    const possibleDeviceTypes = ["LK800", "LK850", "TL401"];

    //const licencesOfDeviceType = useSelector( (state: RootState) => state.deviceGroup.licencesOfDeviceType );

    const licencesOfDeviceType: Licence[] = useSelector(selectDeviceGroupsLicences)



    //va stocker clé-valeur, LK800 pour la cré, licence correspondante en valeur

    const handleChange = (deviceType:any) => {
        dispatch(setDeviceType(deviceType));
        dispatch(getLicensesOfDeviceType(deviceType));
    }

    React.useEffect (() => { 
       
        if(deviceGroups.length === 0){
            dispatch(getDeviceGroups(siteId))
        }
        if (licencesOfDeviceType.length === 0){
            dispatch(getLicensesOfDeviceType(deviceType));
        }
       
    }, [dispatch, deviceGroups, deviceType, licencesOfDeviceType, siteId]);     
        
    return (
        
        <div className="DeviceGroups" data-testid="DeviceGroups">
           
            number of device groups: {deviceGroups.length}
            <br /> <br />
            <ul>
                {/* {deviceGroups.map((device:DeviceGroup)=> <li>{device.name} {device.type}</li>)} */}
                {deviceGroups.map((device:DeviceGroup)=> <li key={device.type}>{device.name} {device.type}</li>)}
            </ul>
            <br />

            <ModaleDevice 
                siteId={siteId} 
                possibleDeviceTypes={possibleDeviceTypes} 
                deviceType={deviceType} 
                sendDataToParentForEdit={handleChange} 
                licencesOfDeviceType={licencesOfDeviceType}
            />
           
        </div>
    ); 
}



