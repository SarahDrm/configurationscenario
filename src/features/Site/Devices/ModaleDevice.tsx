/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Thursday December 31st 2020
 */


import React, { ChangeEvent, FunctionComponent, useState } from "react";
import { Button, Modal } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import { useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import { createDeviceGroup } from "./slice";
import { Licence } from "../../../dataModel/Licence";

interface DeviceProps {
    siteId: string,
    deviceType: string,
    licencesOfDeviceType: Licence[],
    possibleDeviceTypes: string[],
    // sendDataToParentForEdit: any
    sendDataToParentForEdit: (sendDataToParentForEdit: string) => void
  }

  export const ModaleDevice: FunctionComponent<DeviceProps> = ({siteId, deviceType, possibleDeviceTypes, licencesOfDeviceType , sendDataToParentForEdit}) => { 

    const dispatch = useDispatch();
    const [showModal, setShowModal] = useState<boolean>(false);

    const close = () => {   setShowModal(false) }

    const open = () => {    setShowModal(true)  }
  
    const { register, handleSubmit } = useForm();

    

    const onSubmit = (data:any) => {
        dispatch(createDeviceGroup(siteId, data.name, data.type, data.deviceLicence));
    }

    const handleChange = (event: ChangeEvent<HTMLSelectElement>) => {
       // event.target.value.toString()
        //dispatch(setDeviceType(data.deviceType));
        //sendDataToParentForEdit((data.deviceType));
        sendDataToParentForEdit(( event.target.value));
    }
    
    return (
      <div>
        <Button
          onClick={open}
          className="add_button"
        >
          Add device group
        </Button>

         <Modal show={showModal} onHide={close}>
            <Modal.Header closeButton>
                <Modal.Title>Add a new device group</Modal.Title>
            </Modal.Header>
            <Modal.Body>
            <form onSubmit={handleSubmit(onSubmit)}>
          

                <label>Name:</label>
                <input type="text" placeholder="Name" name="name" ref={register({required: true, maxLength: 80})} />
                <br />
                <label>Type:</label>
                {/* <select name="deviceType" ref={register}>
                    <option value="LK800">Tracker LK800</option>
                    <option value="LK850">Tracker LK850</option>
                    <option value="TL401">Tracker TL401</option>
                </select> */}
              
                <select name="type"  
                    // onChange={(e) => {
                    //   handleChange(e)
                    //   }}
                      onChange={handleChange}
                    ref={register}>
                    {possibleDeviceTypes.map((value:string) => (
                        <option key={value} value={value}>Tracker {value}</option>
                    ))}
                </select>

                <br />
                <label>License:</label>
                <select name="deviceLicence" onChange={handleChange} ref={register}>
                    {licencesOfDeviceType.map((value:Licence) => (
                        <option key={value.name} value={value.name}> {value.name}</option>
                    ))}
                </select>
                <br />
                <button>Add</button>
            </form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={close}>
                    Close
                </Button>
                {/* <Button variant="primary" onClick={close}>
                    Save Changes
                </Button> */}
            </Modal.Footer>
        </Modal>
      </div>
    ); 
}

export default ModaleDevice;

