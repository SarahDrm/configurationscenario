/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Thursday December 31st 2020
 */


import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { AppThunk } from '../../../store'
import { DeviceGroup } from '../../../dataModel/DeviceGroup';
import { addDeviceGroupWithApi, getDeviceGroupsFromApi } from './api';

// interface DeviceLicencesMap {
//   [key: string] : Licence[];
// } 

const initialState : any = ({
   error:null,
   pending: true,
   deviceGroups: [], //liste de devicesGroup
   deviceType: 'LK800'
});  

const slice = createSlice({
    name: 'deviceGroup',
    initialState,
    reducers: {
          getDeviceGroupBySiteId (state, action: PayloadAction<DeviceGroup[]> )  {
            state.deviceGroups = action.payload;
          },
          setDeviceTypeAction(state, action: PayloadAction<string>) {
          state.deviceType = action.payload;
          }
    }
});

export const setDeviceType = (deviceType: string): AppThunk => async (dispatch) => {
  try {
     dispatch(slice.actions.setDeviceTypeAction(deviceType))
  } catch (err) {
    console.log(err.toString());
  }
}

export const getDeviceGroups = (siteId: string): AppThunk => async (dispatch) => {
  const deviceLists: DeviceGroup[] = await getDeviceGroupsFromApi(siteId);
  dispatch(slice.actions.getDeviceGroupBySiteId(deviceLists))
}

export const createDeviceGroup = (siteId: string, name: string, deviceType:string, deviceLicence:string): AppThunk => async (dispatch) => {
  addDeviceGroupWithApi(siteId, name, deviceType, deviceLicence );
}

export const {  
  getDeviceGroupBySiteId,
  setDeviceTypeAction
} = slice.actions; 

export default slice.reducer; 
				  