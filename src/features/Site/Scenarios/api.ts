/*
 * Copyright (c) 2021 MyKeeper
 * -----
 * Created Date: Tuesday January 5th 2021
 */

import axios from "axios";
import { api_url } from "../../../api/apiAuth";
import { Scenario } from "../../../dataModel/Scenario";


const feature = "scenario/";


export async function getListScenariosFromApi(siteId: string) : Promise<Scenario[]> {
    const data = {
        siteId: siteId
    };
    try {
        const result = await axios.post(`${api_url}${feature}getScenarioInstanceOfSite`,data);
        return result.data.result
    } catch (err) {
        console.log(err.toString());
        return [];
    }
}

export async function addScenarioConfigWithApi(siteId: string, instanceName: string, scenarioTemplateName:string) : Promise<Scenario[]> {
    const data =   {
        siteId: siteId,
        instanceName: instanceName ,
        scenarioTemplateName: scenarioTemplateName
      }
    try { 
        const result = await axios.post(`${api_url}${feature}createScenarioInstance`,data);
        return result.data.result
      } catch (err) {
        console.log(err.toString());
        return [];
      }
}

export async function getPossibleScenarioLicencesFromApi() : Promise<any[]> {
    try {
        const result = await axios.get(`${api_url}${feature}getScenarioTemplates`);
        return result.data.result
    } catch (err) {
        console.log(err.toString());
        return [];
    }
}
