/*
 * Copyright (c) 2021 MyKeeper
 * -----
 * Created Date: Friday January 1st 2021
 */

import { createSlice, PayloadAction } from "@reduxjs/toolkit"
import { Scenario } from "../../../dataModel/Scenario"
import { AppThunk } from "../../../store"
import { addScenarioConfigWithApi, getListScenariosFromApi, getPossibleScenarioLicencesFromApi } from "./api";


const initialState : any = ({
    error:null,
    pending: true,
    listScenarios: [],
    licencesScenarioPossible: [],
 });  
 
 const slice = createSlice({
     name: 'scenarios',
     initialState,
     reducers: {
           getListScenariosBySiteId (state, action: PayloadAction<Scenario[]> )  {
             state.listScenarios = action.payload;
           },
           getScenarioLicences(state, action: PayloadAction<any[]> )  {
            state.licencesScenarioPossible = action.payload;
          }
     }
 });
 
 
 export const getListScenarios = (siteId: string): AppThunk => async (dispatch) => {
  const scenarioLists: Scenario[] = await getListScenariosFromApi(siteId);
  dispatch(slice.actions.getListScenariosBySiteId(scenarioLists))
}

 export const createScenarioConfig = (siteId: string, instanceName: string, scenarioTemplateName:string): AppThunk => async (dispatch) => {
  addScenarioConfigWithApi(siteId, instanceName, scenarioTemplateName );
}


 // récupérer les licences de scenario possibles (PPM intrusion, PTI)

 export const getPossibleScenarioLicences = (): AppThunk => async (dispatch) => {
  const scenarioLists: any[] = await getPossibleScenarioLicencesFromApi();
  dispatch(slice.actions.getScenarioLicences(scenarioLists))
}
 
 export const { getListScenariosBySiteId, getScenarioLicences } = slice.actions; 
 
 export default slice.reducer; 
                   