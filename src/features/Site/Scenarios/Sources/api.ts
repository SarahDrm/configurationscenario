/*
 * Copyright (c) 2021 MyKeeper
 * -----
 * Created Date: Tuesday January 5th 2021
 */



import axios from "axios";
import { api_url } from "../../../../api/apiAuth";
import { Source } from "../../../../dataModel/Source";



const feature = "scenario/";
//const feature_device = "device/";


export async function getScInstSourceFromApi(siteId: string, sourceId: string) : Promise<Source[]> {
    const data = {
        siteId: siteId,
        scenarioInstanceId: sourceId
    };
    try {
        const result = await axios.post(`${api_url}${feature}getScInstSource`,data);
        return result.data.result
    } catch (err) {
        console.log(err.toString());
        return [];
    }
}


// export async function getDeviceTypeFromApi(type: string) : Promise<string[]> {
//     const data = {
//         type: type
//     };
//     try {
//         const result = await axios.post(`${api_url}${feature_device}getDeviceType`,data);
//         return result.data.result.events
//     } catch (err) {
//         console.log(err.toString());
//         return [];
//     }
// }


export async function addScInstSourceWithApi(siteId: string, scenarioInstanceId: string, deviceGroupId:string, event:string, sourceName: string) : Promise<Source[]> {
    const data =   {
        siteId: siteId, 
        scenarioInstanceId: scenarioInstanceId, 
        deviceGroupId:deviceGroupId,
        event: event,
        sourceName: sourceName
      }
    try { 
        const result = await axios.post(`${api_url}${feature}addScInstSource`,data);
        return result.data.result
      } catch (err) {
        console.log(err.toString());
        return [];
      }
}


// export async function getEventsOfDeviceTypeFromApi(deviceType: string) : Promise<string[]> {
//     const data = {
//         type: deviceType
//     };
//     try {
//         const result = await axios.post(`${api_url}${feature_device}getDeviceType`,data);
//         return result.data.result.events
//     } catch (err) {
//         console.log(err.toString());
//         return [];
//     }
// }