/*
 * Copyright (c) 2021 MyKeeper
 * -----
 * Created Date: Friday January 1st 2021
 */



import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { AppThunk } from '../../../../store'
import { Source } from '../../../../dataModel/Source';
import { addScInstSourceWithApi,  
  //getEventsOfDeviceTypeFromApi, 
  getScInstSourceFromApi } from './api';


const initialState : any = ({
   error:null,
   pending: true,
   scInstSource: [],
   deviceGroupsEvents:[],
   currentDeviceType: ''//,
   //events:[]
});  

const slice = createSlice({
    name: 'sources',
    initialState,
    reducers: {
          getSources (state, action: PayloadAction<Source[]> )  {
            state.scInstSource = action.payload;
            state.pending = false;
           },
          //  getDeviceType (state, action: PayloadAction<string[]> )  {
          //   state.scInstSource = action.payload;
          //   state.pending = false;
          //  },
           setCurrentDeviceTypeAction(state, action: PayloadAction<string>) {
            state.currentDeviceType = action.payload;
            },
            // getEventsOfDeviceTypeTypeAction(state, action: PayloadAction<string[]>) {
            //   state.events = action.payload;
            //   state.pending = false;
            // }
  
    }
});

// gros gros pbl : getEventsOfDeviceType et getDeviceType sont exactement les memes fonctions

// export const getEventsOfDeviceType = (deviceType: string): AppThunk => async (dispatch) => {
//   const events: string[] = await getEventsOfDeviceTypeFromApi(deviceType);
//   dispatch(slice.actions.getEventsOfDeviceTypeTypeAction(events))
// }

export const setCurrentDeviceType = (currentDeviceType: string): AppThunk => async (dispatch) => {
  try {
     dispatch(slice.actions.setCurrentDeviceTypeAction(currentDeviceType))
  } catch (err) {
    console.log(err.toString());
  }
}

// export const getDeviceType = (type: string): AppThunk => async (dispatch) => {
//   const events: string[] = await getDeviceTypeFromApi(type);
//   dispatch(slice.actions.getDeviceType(events))
// }

export const getScInstSource = (siteId: string, sourceId: string): AppThunk => async (dispatch) => {
  const sourceLists: Source[] = await getScInstSourceFromApi(siteId, sourceId);
  dispatch(slice.actions.getSources(sourceLists))
}


export const addScInstSource = (
    siteId: string, 
    scenarioInstanceId: string, 
    deviceGroupId:string,
    event: string,
    sourceName: string
  ): AppThunk => async (dispatch) => {
    addScInstSourceWithApi(siteId, scenarioInstanceId, deviceGroupId, event, sourceName );
}


export const { getSources } = slice.actions; 

export default slice.reducer; 
				  