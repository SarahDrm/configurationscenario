/*
 * Copyright (c) 2021 MyKeeper
 * -----
 * Created Date: Friday January 1st 2021
 */

import React, { ChangeEvent, FunctionComponent, useState } from "react";
import { Button, Modal } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import { useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import { addScInstSource } from "./slice";
import { Scenario } from "../../../../dataModel/Scenario";
import { DeviceGroup } from "../../../../dataModel/DeviceGroup";

interface SourcesProps {
    siteId: string,
    scenario: Scenario,
    deviceGroups: DeviceGroup[],
    currentDeviceType: string,
    sendDataToParentForEdit: (sendDataToParentForEdit: string) => void,
    events:string[]
  }

  export const ModaleSource: FunctionComponent<SourcesProps> = ({
      siteId, 
      scenario,
      deviceGroups,
      currentDeviceType,
      sendDataToParentForEdit,
      events
    }) => { 

    const dispatch = useDispatch();
    const [showModal, setShowModal] = useState<boolean>(false);

    const close = () => { setShowModal(false) }

    const open = () => { setShowModal(true)  }
  
    const { register, handleSubmit } = useForm();

    const handleChange = (event: ChangeEvent<HTMLSelectElement>) => {
         sendDataToParentForEdit(( event.target.value));
     }

    const onSubmit = (data:any) => {
        dispatch(addScInstSource(
            siteId, 
            scenario.scenarioInstanceId, 
            data.deviceGroupId,
            data.event,
            data.sourceName
            )
        );
    }
    
    return (
      <div>
        <Button
          onClick={open}
        >
          Add trigger source
        </Button>

         <Modal show={showModal} onHide={close}>
            <Modal.Header closeButton>
                <Modal.Title>Add new source</Modal.Title>
            </Modal.Header>
            <Modal.Body>
            <form onSubmit={handleSubmit(onSubmit)}>
                <label>Device group:</label>
                <select name="deviceGroupId"  
                 onChange={handleChange}
                    ref={register}> 
                    {deviceGroups.map((value:DeviceGroup) => (
                        <option key={value.name} value={value.type}>{value.name}</option>
                     ))}
                    
                </select>

                <br />
                <label>Event:</label>
                {/* événements que le groupe de device peut émettre*/}
                <select name="event"  
                onChange={handleChange}
                    ref={register}> 
                    {events.map((value:string) => (
                        <option key={value} value={value}>{value}</option>
                     ))}
                   
                </select>
                <br />
                <label>Source name:</label>
                <input type="text" placeholder="Name" name="sourceName" ref={register({required: true, maxLength: 80})} />

                <br />
                <button>Add</button>
            </form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={close}>
                    Close
                </Button>
            </Modal.Footer>
        </Modal>
      </div>
    ); 
}

export default ModaleSource;


