/*
 * Copyright (c) 2021 MyKeeper
 * -----
 * Created Date: Friday January 1st 2021
 */

import { MDBIcon } from 'mdbreact';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Breadcrumb, BreadcrumbItem } from 'reactstrap';
import { Scenario } from '../../../../dataModel/Scenario';
import { Source } from '../../../../dataModel/Source';
import { RootState } from '../../../../rootReducer';
import { getEventsOfDeviceType, selectEvents } from './Events/slice';
import ModaleSource from './ModaleSource';
import { 
  //getEventsOfDeviceType, 
  getScInstSource, setCurrentDeviceType } from './slice';


interface ScenarioProps {
    scenario: Scenario,
    props:any
  }
    //TODO voir comment supprimer ce props
    //export const ScenarioConfigurationContent: FunctionComponent<any> = ({scenario, props}) => {   
    export const ScenarioConfigurationContent = (props:any) => {   
 
     const dispatch = useDispatch();

     const scenario = props.location.state.scenario
     const siteId = props.location.state.siteId

     const sources = useSelector( (state: RootState) => state.sources.scInstSource );
     const pending = useSelector( (state: RootState) => state.sources.pending );

     const deviceGroups = useSelector( (state: RootState) => state.deviceGroup.deviceGroups );

     const currentDeviceType = useSelector( (state: RootState) => state.sources.currentDeviceType );

     //const events = useSelector( (state: RootState) => state.sources.events );

     const events: string[] = useSelector(selectEvents)

     
     React.useEffect (() => { 
       if (pending){
        dispatch(getScInstSource(siteId, scenario.scenarioInstanceId));
       }
       if (pending){
        dispatch(getEventsOfDeviceType(currentDeviceType));
       }
    }, 
    ); 

    const handleChange = (currentDeviceType:any) => {
      dispatch(setCurrentDeviceType(currentDeviceType));
      dispatch(getEventsOfDeviceType(currentDeviceType));
  }
        
    return (
        <div className= "ScenarioConfigurationContent" data-testid="ScenarioConfigurationContent">


        <Breadcrumb>
          <BreadcrumbItem><a href="/Sites"><MDBIcon icon="home" /> Home</a></BreadcrumbItem>
          <BreadcrumbItem><a href={`/SiteElements/${siteId}`}>Sites</a></BreadcrumbItem>
          <BreadcrumbItem active><MDBIcon icon="wrench" />  Scenario configurations</BreadcrumbItem>
        </Breadcrumb>


            {/* {props.location.state.scenario} */}
            <br /><br />
            <h2>Scenario de configuration pour le site </h2>
            <h6>(id du site: {siteId} )</h6>
            <br /><br />
              Instance "{scenario.instanceName}" : template "{scenario.scenarioTemplateName}" 

            <br /> <br />
              <button>Generate summary</button>

              <br /><br />
              <h3>Sources</h3>
              <ul>
                {sources.map((source:Source)=> <li key={source.sourceName}>{source.event} - {source.sourceName}</li>)}
             </ul>

              {/* <button>Add trigger source</button> */}
              {/*
                needed elements
              
              */}
              <ModaleSource 
                siteId={siteId} 
                scenario={scenario}
                deviceGroups={deviceGroups}
                currentDeviceType={currentDeviceType}
                sendDataToParentForEdit={handleChange} 
                events={events}
                />

                currentDeviceType : {currentDeviceType}

              <br />
              <h3>Targets</h3>

              <button>Add target</button>
              <h5>Level 1</h5>

              <h6>Devices</h6>

              <h6>Contact lists</h6>

              <br />
              <h5>Level 2</h5>


              <h6>Devices</h6>

              <h6>Contact lists</h6>

            
            
            
        </div>
    ); 
}



