/*
 * Copyright (c) 2021 MyKeeper
 * -----
 * Created Date: Wednesday January 6th 2021
 */

import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../../../../../rootReducer";
import { AppThunk } from "../../../../../store";
import { getEventsOfDeviceTypeFromApi } from "./api";


const initialState : any = ({
    events:[]
 }); 



const slice = createSlice({
    name: 'sourcesEvents',
    initialState,
    reducers: {
            getEventsOfDeviceTypeTypeAction(state, action: PayloadAction<string[]>) {
              state.events = action.payload;
            }
  
    }
});


export const getEventsOfDeviceType = (deviceType: string): AppThunk => async (dispatch) => {
    const events: string[] = await getEventsOfDeviceTypeFromApi(deviceType);
    dispatch(slice.actions.getEventsOfDeviceTypeTypeAction(events))
  }



  export const { getEventsOfDeviceTypeTypeAction } = slice.actions; 

  export const selectEvents = (state: RootState): string[] => state.sourcesEvents.events

  export default slice.reducer; 
		
  
  