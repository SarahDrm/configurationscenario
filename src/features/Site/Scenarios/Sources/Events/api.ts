/*
 * Copyright (c) 2021 MyKeeper
 * -----
 * Created Date: Wednesday January 6th 2021
 */

import axios from "axios";
import { api_url } from "../../../../../api/apiAuth";

const feature = "device/";


export async function getEventsOfDeviceTypeFromApi(deviceType: string) : Promise<string[]> {
    const data = {
        type: deviceType
    };
    try {
        const result = await axios.post(`${api_url}${feature}getDeviceType`,data);
        return result.data.result.events
    } catch (err) {
        console.log(err.toString());
        return [];
    }
}