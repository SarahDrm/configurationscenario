/*
 * Copyright (c) 2021 MyKeeper
 * -----
 * Created Date: Friday January 1st 2021
 */


import React, { FunctionComponent } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../../rootReducer';
import { getListScenarios, getPossibleScenarioLicences } from './slice'
import { Scenario } from '../../../dataModel/Scenario';
import ModaleScenario from './ModaleScenario';
import { Link } from 'react-router-dom';

interface ScenarioProps {
    siteId: string
  }
    
export const ListScenarios: FunctionComponent<ScenarioProps> = ({siteId}) => {  

    const dispatch = useDispatch();
       
    const listScenarios = useSelector( (state: RootState) => state.scenarios.listScenarios );

    const licencesScenarioPossible = useSelector( (state: RootState) => state.scenarios.licencesScenarioPossible );

    React.useEffect (() => { 
      if(listScenarios.length === 0){
        dispatch(getListScenarios(siteId))
      }
      if(listScenarios.length === 0){
        dispatch(getPossibleScenarioLicences())
      }
    }, 
    );     
        
    return (
        <div className= "ListScenarios" data-testid="ListScenarios">
            
      
            number of scenario configurations: {listScenarios.length}
           
            <br />  <br />
            <ul>
            {listScenarios.map((scenario:Scenario) =>
            <li key={scenario.scenarioInstanceId}>
              <Link to={{
                pathname: '/ScenarioConfig/',
                state: {
                   scenario: scenario,
                   siteId: siteId
                }
              }}>
                   Instance "{scenario.instanceName}" : template "{scenario.scenarioTemplateName}"
                </Link>
                </li>
              )}
            </ul>

            <ModaleScenario siteId={siteId} licencesScenarioPossible={licencesScenarioPossible}/>

        </div>
    ); 
}



