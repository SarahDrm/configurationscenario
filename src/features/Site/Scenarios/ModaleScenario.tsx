/*
 * Copyright (c) 2021 MyKeeper
 * -----
 * Created Date: Friday January 1st 2021
 */


import React, { FunctionComponent, useState } from "react";
import { Button, Modal } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import { useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import { createScenarioConfig } from "./slice";

interface SiteProps {
    siteId: string
    licencesScenarioPossible: string[]
  }

  export const ModaleContact: FunctionComponent<SiteProps> = ({siteId, licencesScenarioPossible}) => { 

    const dispatch = useDispatch();
    const [showModal, setShowModal] = useState<boolean>(false);

    const close = () => { setShowModal(false) }

    const open = () => { setShowModal(true)  }
  
    const { register, handleSubmit } = useForm();

    const onSubmit = (data:any) => {
        dispatch(createScenarioConfig(siteId, data.instanceName, data.scenarioTemplateName));
    }
    
    return (
      <div>
        <Button
          onClick={open}
          className="add_button"
        >
          Add scenario config
        </Button>

         <Modal show={showModal} onHide={close}>
            <Modal.Header closeButton>
                <Modal.Title>Add new scenario configuration</Modal.Title>
            </Modal.Header>
            <Modal.Body>
            <form onSubmit={handleSubmit(onSubmit)}>
                <label>Config name:</label>
                <input type="text" placeholder="Name" name="instanceName" ref={register({required: true, maxLength: 80})} />

                <select name="scenarioTemplateName"  
                    ref={register}>
                    {licencesScenarioPossible.map((value:any) => (
                        <option key={value.name} value={value.name}>{value.name}</option>
                    ))}
                </select>

                <button>Add</button>
            </form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={close}>
                    Close
                </Button>
            </Modal.Footer>
        </Modal>
      </div>
    ); 
}

export default ModaleContact;

