/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Monday December 28th 2020
 */

import React, { FunctionComponent, Suspense } from 'react';
import { Container } from 'react-bootstrap';
import { useParams } from 'react-router-dom';
import { ContactLists } from './Contacts/contactList';
import { DeviceGroups } from './Devices/DeviceGroups';
import { ListScenarios } from './Scenarios/ListScenarios';
import { Card, CardTitle, CardGroup, CardSubtitle, CardBody, CardHeader, Breadcrumb, BreadcrumbItem } from 'reactstrap';
import './SiteElements.scss'
import { MDBIcon } from 'mdbreact';


interface ParamTypes {
    siteId: string,
    siteName: string
  }
    
    export const SiteElements: FunctionComponent = () => {   
 
      const { siteId } = useParams<ParamTypes>()
      const { siteName } = useParams<ParamTypes>()
      console.log("siteName")
      console.log(siteName)
     
     // const [collapse, setCollapse] = useState(false); 
      //const toggle = () => setCollapse(!collapse);
      
    return (
      
        <div className= "SiteElements" data-testid="SiteElements">
          <Suspense fallback={<h1>Chargement du profil...</h1>}>

                  <Breadcrumb>
                    <BreadcrumbItem><a href="/Sites"><MDBIcon icon="home" /> Home</a></BreadcrumbItem>
                    <BreadcrumbItem active>Sites</BreadcrumbItem>
                  </Breadcrumb>

                      <h2>  Bienvenue sur la page de gestion du site {siteName}</h2>
                      <h6>(id du site: {siteId} )</h6>

                      
                      {/* <Container>
                        <Row>
                          <Col id="col1"><ContactLists siteId={siteId}/></Col>
                          <Col id="col2"><DeviceGroups siteId={siteId}/></Col>
                          <Col id="col3"><ListScenarios siteId={siteId}/></Col>
                        </Row>
                      </Container> */}

                      <Container>
                      <CardGroup>
                        <Card>
                          {/* <Fragment>
                            <MDBBtn color="primary" onClick={toggle}>
                              <MDBIcon icon="magic" className="mr-1" /> Contact List
                            </MDBBtn>
                          </Fragment> */}
                          {/* <Collapse isOpen={collapse}> */}
                            <Card>
                            <CardHeader>Contact list</CardHeader>
                              <CardBody>
                              <CardTitle tag="h5">Contact lists </CardTitle>
                              <CardSubtitle tag="h6" className="mb-2 text-muted">for this site</CardSubtitle>
                              <Suspense fallback={<h1>Chargement du profil...</h1>}>
                                <ContactLists siteId={siteId}/>
                              </Suspense>
                              
                              </CardBody>
                            </Card>
                          {/* </Collapse> */}
                        </Card>
                        <Card>
                        <CardHeader>Device groups</CardHeader>
                          <CardBody>
                            <CardTitle tag="h5">List of device groups</CardTitle>
                            <CardSubtitle tag="h6" className="mb-2 text-muted">for this site</CardSubtitle>
                            <DeviceGroups siteId={siteId}/>
                          </CardBody>
                        </Card>
                        <Card>
                        <CardHeader>Scenario configurations</CardHeader>
                          <CardBody>
                            <CardTitle tag="h5">Scenario configurations</CardTitle>
                            <CardSubtitle tag="h6" className="mb-2 text-muted">for this site</CardSubtitle>
                            <ListScenarios siteId={siteId}/>
                          </CardBody>
                        </Card>
                      </CardGroup>
                      </Container>

                    
                      </Suspense>
        </div>
    ); 
}



