/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Tuesday December 29th 2020
 */

import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { AppThunk } from '../../../store'
import { ContactList } from '../../../dataModel/ContactList';
import { addContactListWithApi, getContactListsFromApi } from './api';


const initialState : any = ({
  contactList: []
});  

const slice = createSlice({
    name: 'contactList',
    initialState,
    reducers: {
        getContactListBySiteId (state, action: PayloadAction<ContactList[]> )  {
            state.contactList = action.payload;
          }
    }
});


export const getContactList = (siteId: string): AppThunk => async (dispatch) => {
 
  const contactLists: ContactList[] = await getContactListsFromApi(siteId);
  
  dispatch(slice.actions.getContactListBySiteId(contactLists))
}


export const createContactList = (siteId: string, name: string, listType:string): AppThunk => async (dispatch) => {
  // TODO: renvoyer le contactlist et le pusher au state existant
  addContactListWithApi(siteId, name, listType);
}

export const { getContactListBySiteId } = slice.actions; 

export default slice.reducer; 
				  