/*
 * Copyright (c) 2021 MyKeeper
 * -----
 * Created Date: Monday January 4th 2021
 */

import axios from "axios";
import { api_url } from "../../../api/apiAuth";
import { ContactList } from "../../../dataModel/ContactList";

const feature = "site/";

export async function getContactListsFromApi(siteId: string) : Promise<ContactList[]> {
    const data = {
        siteId: siteId
    };
    try {
        const result = await axios.post(`${api_url}${feature}getContactListBySiteId`,data);
        return result.data.result
    } catch (err) {
        console.log(err.toString());
        return [];
    }
}


export async function addContactListWithApi(siteId: string, name: string, listType: string) : Promise<ContactList[]> {
    const data =   {
        siteId: siteId,
        name: name ,
        listType: listType
      }
    try { // voir comment transformer en json
        const result = await axios.post(`${api_url}${feature}createContactList`,data);
        // TODO CE TRUC NE RETOURNE RIEN NORMALEMENT
        return result.data.result
      } catch (err) {
        console.log(err.toString());
        return [];
      }
}

// export const createContactList = (siteId: string, name: string, listType:string): AppThunk => async (dispatch) => {
//     try { // voir comment transformer en json
//       const data =   {
//         "siteId": siteId,
//         "name": name ,
//         "listType": listType
  
//       }
//       axios.post(`https://dje.mykeeper.link/site/createContactList`,  data)
//     //   .then(res => dispatch(slice.actions.getContactListBySiteId(res.data.result)))
//     } catch (err) {
//       dispatch(getContactListFailed(err.toString()))
//     }
//   }