/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Monday December 28th 2020
 */

import React, { FunctionComponent } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../../rootReducer';
import { getContactList } from './slice'
import './contactList.scss'
import { ContactList } from '../../../dataModel/ContactList';
import ModaleContact from './ModaleContact';

interface ContactsProps {
    siteId: string
  }
    
export const ContactLists: FunctionComponent<ContactsProps> = ({siteId}) => {  

    const dispatch = useDispatch();
       
    const contactList = useSelector( (state: RootState) => state.contactList.contactList );
    //const contactListPending = useSelector( (state: RootState) => state.contactList.pending );

    React.useEffect (() => { 
        if(contactList.length === 0) {
            dispatch(getContactList(siteId))
        }
    }, 
    );     

    //const { register, handleSubmit } = useForm();

    // const onSubmit = (data:any) => {
    //    dispatch(createContactList(siteId, data.name, data.listType));
    // }
        
    return (
        <div className= "ContactLists" data-testid="ContactLists">
           
            number of contact lists: {contactList.length}
            <br />
            <br />
            <ul>
                {contactList.map((list:ContactList)=> <li key={list.key}>{list.name} {list.listType}</li>)}
            </ul>
           
            
            {/* <form onSubmit={handleSubmit(onSubmit)}>
                <input type="text" placeholder="Name" name="name" ref={register({required: true, maxLength: 80})} />
                <input type="text" placeholder="Typ" name="listType" ref={register({required: true, maxLength: 100})} />
                <button>Add</button>
            </form> */}

            <ModaleContact siteId={siteId}/>

        </div>
    ); 
}



