/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Thursday December 31st 2020
 */

import React, { FunctionComponent, useState } from "react";
import { Button, Modal } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import { useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import { createContactList } from "./slice";
import './Modale.scss'

interface SiteProps {
    siteId: string
  }

  export const ModaleContact: FunctionComponent<SiteProps> = ({siteId}) => { 

    const dispatch = useDispatch();
    const [showModal, setShowModal] = useState<boolean>(false);

    const close = () => {   setShowModal(false) }

    const open = () => {    setShowModal(true)  }
  
    const { register, handleSubmit } = useForm();

    const onSubmit = (data:any) => {
        dispatch(createContactList(siteId, data.name, data.listType));
    }
    
    return (
      <div>
        <Button
          onClick={open}
          className="add_button"
        >
          Add contact list
        </Button>

         <Modal show={showModal} onHide={close}>
            <Modal.Header closeButton>
                <Modal.Title>Add new contact list</Modal.Title>
            </Modal.Header>
            <Modal.Body>
            <form onSubmit={handleSubmit(onSubmit)}>
                <input type="text" placeholder="Name" name="name" ref={register({required: true, maxLength: 80})} />
                <input type="text" placeholder="Type" name="listType" ref={register({required: true, maxLength: 100})} />
                <button>Add</button>
            </form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={close} >
                    Close
                </Button>
                {/* <Button variant="primary" onClick={close}>
                    Save Changes
                </Button> */}
            </Modal.Footer>
        </Modal>
      </div>
    ); 
}

export default ModaleContact;

