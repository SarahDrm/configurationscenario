/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Monday October 5th 2020
 */

import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { AppThunk } from '../../store';
import { Site } from '../../dataModel/Site';
import { getSitesFromApi } from './api';



const initialState : any = ({
   error:null,
   pending: true,
   sites: []
});  

const slice = createSlice({
    name: 'site',
    initialState,
    reducers: {
       
        getSiteSuccess (state, action: PayloadAction<Site[]> )  {
            state.pending = false;
            state.sites = action.payload;
          }
     
    }
});


export const getSites = (): AppThunk => async (dispatch) => {
  const siteLists: Site[] = await getSitesFromApi();
  dispatch(slice.actions.getSiteSuccess(siteLists))
}

export const { getSiteSuccess } = slice.actions; 

export default slice.reducer; 
				  
