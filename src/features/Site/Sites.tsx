/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Saturday December 26th 2020
 */

import React, { Suspense } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { getSites } from './slice'
import { RootState } from '../../rootReducer';
import { Site } from '../../dataModel/Site';
import { Link } from 'react-router-dom';
import { Breadcrumb, BreadcrumbItem } from 'reactstrap';
import { MDBIcon } from 'mdbreact';

export default function Sites(props:any): JSX.Element {
    
      const dispatch = useDispatch();
       
      const sites = useSelector( (state: RootState) => state.site.sites );

      React.useEffect (() => { // remplace componentDidMount componentDidUpdate
            if(sites.length === 0) {
              dispatch(getSites())
            }
        }, 
        );     

       
      return (
            <div className= "Sites" data-testid="Sites">

              
              <Breadcrumb>
                <BreadcrumbItem active> 
                 <MDBIcon icon="home" />  
                  Home
                </BreadcrumbItem>
              </Breadcrumb>

              <br /><br /><br /><br />

               <Suspense fallback={<h1>Chargement du profil...</h1>}>
               {  (sites !== undefined )? sites.length :''}
                    
                    {  (sites !== undefined )?  
                    <ul>
                      {sites.map((site:Site) =>
                      <li key={site.key}>
                      <Link to={{
                        pathname: '/SiteElements/'+site.key,
                        state: {
                          siteId: site.key,
                          siteName: site.name
                        }
                      }}>{site.name}</Link> 
                      </li>
                      )}
                    </ul> :''}
                    
                      <br></br>
                  
                     {/* <Modal /> */}
                 
               </Suspense>

         
          
            </div>
      ); 
  }



