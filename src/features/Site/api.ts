/*
 * Copyright (c) 2021 MyKeeper
 * -----
 * Created Date: Tuesday January 5th 2021
 */

import axios from "axios";
import { api_url, auth_user } from "../../api/apiAuth";
import { Site } from "../../dataModel/Site";



const feature = "site/";

export async function getSitesFromApi() : Promise<Site[]> {
    try {
        const result = await axios.post(`${api_url}${feature}getSite`,auth_user);
        return result.data.result
    } catch (err) {
        console.log(err.toString());
        return [];
    }
}
