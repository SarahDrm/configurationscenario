import './App.scss';
import React from 'react';
import Sites from './features/Site/Sites';
import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom';
import { SiteElements } from './features/Site/SiteElements';
import { ScenarioConfigurationContent } from './features/Site/Scenarios/Sources/ScenarioConfigurationContent';


function App() {
  return (
    <div className="App">
      <header>
          <h1>Configuration de scenario</h1>
      </header> 
         {/* <ProgressBar activeStepIndex={1} /> */}
        
        <Router>
       
          <Switch>
          
            <Route exact={true} path="/" component={() => <Redirect to={`/Sites`}/>} />
            <Route path="/Sites" component={Sites} />
            <Route path="/SiteElements/:siteId" component={SiteElements} />
            <Route path="/ScenarioConfig" component={ScenarioConfigurationContent} />
            {/* <Route
              path="ScenarioConfig/:scenarioInstanceId"
              render={({ scenario }) => <ScenarioConfigurationContent scenario={scenario} />}
            /> */}
          </Switch>
           
        </Router>  
    </div>
  );
}

export default App;
