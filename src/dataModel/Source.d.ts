/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Tuesday December 29th 2020
 */

export type Source = {
    siteId: string,
    scenarioInstanceId: string,
    sourceName: string,
    deviceGroupId: string,
    event: string
  }

