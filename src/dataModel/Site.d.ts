/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Monday December 28th 2020
 */

export type Site = {
    type: string,
    key: string,
    name: string
  }

