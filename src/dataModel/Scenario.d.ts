/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Tuesday December 29th 2020
 */


export type Scenario = {
    siteId: string,
    scenarioInstanceId: string,
    scenarioTemplateName: string,
    instanceName: string
  }

