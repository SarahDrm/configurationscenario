/*
 * Copyright (c) 2020 MyKeeper
 * -----
 * Created Date: Tuesday December 29th 2020
 */


export type Target = {
    scenarioInstanceId: string,
    targetType: string,
    targetId: string,
    level: string,
    siteId: string
  }
